

process.env.NODE_ENV = 'localDevelopment';
config = require('config');
dbConnection = require('./routes/database');
constant = require('./routes/constant');


var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var http = require('http');
var app = express();


var routes = require('./routes/index');
var userDriverloginsignup = require('./routes/userDriverloginsignup');



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.get('/',routes);
app.get('/login_user',routes);
app.get('/changePassword',routes);
app.post('/addUser',userDriverloginsignup);
app.post('/loginUser',userDriverloginsignup);
app.post('/addDriver',userDriverloginsignup);
app.post('/adminLogin',userDriverloginsignup);
app.post('/driverStatus',userDriverloginsignup);
app.post('/delete_drivers',userDriverloginsignup);
app.post('/searchDriver',userDriverloginsignup);
app.post('/userRecoveryMail',userDriverloginsignup);
app.post('/driverRecoveryMail',userDriverloginsignup);
app.post('/userChangePassword',userDriverloginsignup);
app.post('/driverChangePassword',userDriverloginsignup);
app.post('/userNewPassword',userDriverloginsignup);
app.post('/driverNewPassword',userDriverloginsignup);
app.post('/verifyDriver',userDriverloginsignup);
app.post('/notVerifyDriver',userDriverloginsignup);
app.post('/accept_order',userDriverloginsignup);
app.post('/reject_order',userDriverloginsignup);
app.post('/nearestDriver',userDriverloginsignup);
//app.post('/register_user', users);
// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

http.createServer(app).listen(config.get('PORT'), function () {
    console.log("Express server listening on port " + config.get('PORT'));
});

//module.exports = app;
