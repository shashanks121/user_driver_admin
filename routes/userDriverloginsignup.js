/**
 * Created by Shashank on 29-10-2015.
 */
var express = require('express');
var router = express.Router();
var md5 = require('md5');
var async = require('async');
var func = require('./commonFunction');
var sendResponse = require('./sendResponse');
router.post('/addUser',function(req,res){
    var firstName = req.body.first_name;
    var lastName = req.body.last_name;
    var email = req.body.email;
    var password = req.body.password;
    var tablename="useregister";
    var confirmPassword=req.body.confirm_password;
    var manValues = [firstName, lastName, email, password,confirmPassword];
        async.waterfall([
                function(callback) {
                  func.checkBlank(res,manValues,callback);
                },
                 function(callback) {
                    func.confirmPassword(res,password,confirmPassword,callback);
                 },
                function(callback) {
                  func.checkEmailAvailability(res,email,tablename,callback)
                }],
                function(err) {
                  var loginTime = new Date();
                  var accessToken = func.encrypt(email + loginTime);
                  var encryptPassword = md5(password);
                  var sql = "INSERT INTO useregister (first_name, last_name, email, password,updated_time,accessToken) VALUES (?,?,?,?,?,?)";
                  var values = [firstName, lastName, email, encryptPassword, loginTime,accessToken];

                  dbConnection.Query(res, sql, values, function (userInsertResult) {
                     if(userInsertResult){
                       var data = {
                        access_token:accessToken.substring(0,56),
                        first_name: firstName,
                        last_name: lastName,
                        status:"user registered"
                       };
                       sendResponse.sendSuccessData(data, res);
                     }
                     else{
                       sendResponse.somethingWentWrongError(res);
                     }
                  });
                }
        );
    }
);
router.post('/loginUser',function(req,res) {
        var email = req.body.email;
        var password = req.body.password;
        var tablename = "useregister";
        var encryptPassword = md5(password);
        var manValues=[email,password];
        async.waterfall([
                function(callback) {
                    func.checkBlank(res, manValues, callback);
                },
                function(callback) {
                  func.checkingEmailPassword(res, email, tablename, encryptPassword, callback);
                }],
                 function(err,accesstoken) {
                   sendResponse.sendSuccessData(accesstoken,res);
                 }
        );
    }
    );
router.post('/addDriver',function(req,res){
    var firstName = req.body.first_name;
    var lastName = req.body.last_name;
    var email = req.body.email;
    var password = req.body.password;
    var tablename="driveregister";
    var latitude=req.body.latitude;
    var longitude=req.body.longitude;
    var deviceType=req.body.device_type;
    var deviceId=req.body.device_token;
    var manValues = [firstName, lastName, email, password,latitude,longitude,deviceType,deviceId];
    async.waterfall([
            function(callback) {
                func.checkBlank(res,manValues,callback);
            },
            function(callback) {
                func.checkEmailAvailability(res,email,tablename,callback);
            }],
            function(err) {
               var loginTime = new Date();
               var accessToken = func.encrypt(email + loginTime);
              var encryptPassword = md5(password);
              var status = false;
              var sql = "INSERT INTO driveregister (first_name,last_name, email,password,updated_time,accessToken,status,latitude,longitude,device_type,device_token) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
              var values = [firstName, lastName, email, encryptPassword, loginTime,accessToken,status,latitude,longitude,deviceType,deviceId];

              dbConnection.Query(res, sql, values, function (userInsertResult) {
                if(userInsertResult) {
                    var data = {
                        access_token: accessToken.substring(0, 56),
                        first_name: firstName,
                        last_name: lastName,
                        status: "not verified yet"
                    };
                    sendResponse.sendSuccessData(data, res);
                }
                 else{
                   sendResponse.somethingWentWrongError(res);
                }
              });
            }
    ) ;
});

router.post('/adminLogin',function(req,res) {
    var email = req.body.email;
    var password = req.body.password;
    var tablename = "adminregister";
    var manValues=[email,password];
    async.waterfall([
        function(callback) {
            func.checkBlank(res,manValues,callback);
        },
            function (callback) {
              func.checkingEmailPassword(res, email, tablename, password, callback);
            }],
        function (err, accessToken) {
            var sql = "SELECT user_Id,first_name, last_name, email,updated_time,status,latitude,longitude,device_type FROM driveregister";
            dbConnection.Query(res, sql, [], function (userInsertResult) {
                    var data = {
                        driverdetails: userInsertResult,
                        adminAccessToken: accessToken
                    };
                    sendResponse.sendSuccessData(data, res);
                }
            );
        }
    );
});
router.post('/driverStatus',function(req,res){
    var adminToken=req.body.admintoken;
    var driverID=req.body.driverId;
    var admintable="adminregister";
    var driverEmail="";
    var manValues=[adminToken,driverID];
    async.waterfall([
            function(callback) {
                func.checkBlank(res,manValues,callback);
            },
            function(callback) {
                func.checkKey(res,adminToken,admintable,callback);
            }],
        function(err,userID) {
            var selected_email="select email,status from driveregister where user_Id='"+driverID+"' LIMIT 1";
            dbConnection.Query(res,selected_email,[], function (userResponse) {
                if(userResponse.length){
                    driverEmail=userResponse;
                    var data={
                        driveremail:driverEmail[0].email,
                        status:driverEmail[0].status
                    };
                    console.log(data);
                    sendResponse.sendSuccessData(data,res);
                }
                else{
                    sendResponse.somethingWentWrongError(res);
                }
            });
        }
    );
});
router.post('/delete_drivers',function(req,res){
    var adminToken=req.body.admintoken;
    var driverID=req.body.driverId;
    var admintable="adminregister";
    var driverEmail="";
    var manValues=[adminToken,driverID];
    async.waterfall([
        function(callback) {
            func.checkBlank(res,manValues,callback);
        },
        function(callback) {
        func.checkKey(res,adminToken,admintable,callback);
        }],
        function(err,userID) {
            var selected_email="select email from driveregister where user_Id='"+driverID+"' LIMIT 1";
            dbConnection.Query(res,selected_email,[], function (userResponse) {
                driverEmail=userResponse;
            });
            var deleteDriver="UPDATE driveregister SET status =false WHERE user_Id='"+driverID+"'LIMIT 1";
            dbConnection.Query(res,deleteDriver,[], function (userResponse) {
              if(userResponse.affectedRows){
                var data={
                driveremail:driverEmail,
                status:0
                };
               sendResponse.sendSuccessData(data,res);
              }
              else{
                sendResponse.somethingWentWrongError(res);
              }
            });
        }
    );
});
router.post('/userRecoveryMail',function(req,res){
    var email=req.body.email;
    var arr=[email];
    var tablename="useregister";
    async.waterfall([
        function(callback) {
          func.checkBlank(res,arr,callback);
        }],
        function(err) {
          func.sendRecoveryMail(res,email,tablename);
        }
    );
});
router.post('/driverRecoveryMail',function(req,res){
    var email=req.body.email;
    var arr=[email];
    var tablename="driveregister";
    async.waterfall([
        function(callback) {
          func.checkBlank(res,arr,callback);
        }],
        function(err) {
          func.sendRecoveryMail(res,email,tablename);
        }
    );
});
router.post('/userChangePassword',function(req,res){
    var tempToken=req.body.Key;
    var new_password=req.body.new_password;
    var confirm_password=req.body.confirm_password;
    var encryptPassword=md5(new_password);
    var tablename="useregister";
    var manValues=[tempToken,new_password,confirm_password];
    async.waterfall([
        function(callback) {
          func.checkBlank(res,manValues,callback);
        },
        function(callback) {
          func.confirmPassword(res,new_password,confirm_password,callback);
        },
        function(callback) {
          func.checkTempToken(res,tempToken,tablename,callback);
        }],
        function(err,userID) {
          func.changePassword(res,encryptPassword,userID,tablename);
        }
    );
});
router.post('/driverChangePassword',function(req,res){
    var tempToken=req.body.Key;
    var new_password=req.body.new_password;
    var confirm_password=req.body.confirm_password;
    var tablename="driveregister";
    var encryptPassword=md5(new_password);
    var manValues=[tempToken,new_password,confirm_password];
    async.waterfall([
        function(callback) {
          func.checkBlank(res,manValues,callback);
        },
        function(callback) {
          func.confirmPassword(res,new_password,confirm_password,callback);
        },
        function(callback) {
          func.checkTempToken(res,tempToken,tablename,callback);
        }],
        function(err,userID) {
          func.changePassword(res,encryptPassword,userID,tablename);
        }
    );
});
router.post('/userNewPassword',function(req,res){
   var tokenKey=req.body.accessToken;
    var oldPassword=req.body.old_password;
    var newPassword=req.body.new_password;
    var confirmPassword=req.body.confirm_password;
    var oldEncryptPassword=md5(oldPassword);
    var newEncryptPassword=md5(newPassword);
    var tablename="useregister";
    var manValues=[tokenKey,oldPassword,newPassword,confirmPassword];
    async.waterfall([
        function(callback) {
          func.checkBlank(res,manValues,callback);
        },
        function(callback) {
          func.confirmPassword(res,newPassword,confirmPassword,callback);
        },
        function(callback) {
          func.checkKey(res,tokenKey,tablename,callback);
        }],
        function(err,userId) {
          func.oldPasswordUpdate(res,userId,oldEncryptPassword,newEncryptPassword,tablename);
        }
    );
});
router.post('/driverNewPassword',function(req,res){
    var tokenKey=req.body.accessToken;
    var oldPassword=req.body.old_password;
    var newPassword=req.body.new_password;
    var confirmPassword=req.body.confirm_password;
    var oldEncryptPassword=md5(oldPassword);
    var newEncryptPassword=md5(newPassword);
    var tablename="driveregister";
    var manValues=[tokenKey,oldPassword,newPassword,confirmPassword];
    async.waterfall([
        function(callback) {
          func.checkBlank(res,manValues,callback);
        },
        function(callback) {
          func.confirmPassword(res,newPassword,confirmPassword,callback);
        },
        function(callback) {
          func.checkKey(res,tokenKey,tablename,callback);
        }],
        function(err,userId) {
          func.oldPasswordUpdate(res,userId,oldEncryptPassword,newEncryptPassword,tablename);
        }
    );

});
router.post('/verifyDriver',function(req,res){
    var adminToken=req.body.admintoken;
    var driverID=req.body.driverId;
    var admintable="adminregister";
    var driverEmail="";
    var manValues=[adminToken,driverID];
    var selected_email="select email from driveregister where user_Id='"+driverID+"' LIMIT 1";
    dbConnection.Query(res,selected_email,[], function (userResponse) {
        driverEmail=userResponse;
    });
    async.waterfall([
        function(callback) {
            func.checkBlank(res,manValues,callback);
        },
        function(callback) {
            func.checkKey(res,adminToken,admintable,callback);
        }],
        function(err,userID) {
          var update="UPDATE driveregister SET status =TRUE WHERE user_Id='"+driverID+"'LIMIT 1";
          dbConnection.Query(res,update,[], function (userResponse) {
             if(userResponse.affectedRows){
               var data={
                driveremail:driverEmail,
                status:1
              };
               sendResponse.sendSuccessData(data,res);
             }
              else{
                 sendResponse.somethingWentWrongError(res);
             }
          });
        }
    );
});
router.post('/notVerifyDriver',function(req,res){
    var adminToken=req.body.admintoken;
    var driverID=req.body.driverId;
    var admintable="adminregister";
    var driverEmail="";
    var manValues=[adminToken,driverID];
    async.waterfall([
        function(callback) {
            func.checkBlank(res,manValues,callback);
        },
        function(callback) {
            func.checkKey(res,adminToken,admintable,callback);
        }],
        function(err,userID) {
            var selected_email="select email from driveregister where user_Id='"+driverID+"' LIMIT 1";
            dbConnection.Query(res,selected_email,[], function (userResponse) {
                driverEmail=userResponse;
            });
            var update="UPDATE driveregister SET status =2 WHERE user_Id='"+driverID+"'LIMIT 1";
            dbConnection.Query(res,update,[], function (userResponse) {
              if(userResponse.affectedRows){
                var data={
                driveremail:driverEmail,
                status:2
                };
               sendResponse.sendSuccessData(data,res);
              }
              else{
                 sendResponse.somethingWentWrongError(res);
              }
          });
        }
    );
});
router.post('/searchDriver',function(req,res){
    var key=req.body.access_token;
    var lat=req.body.latitude;
    var long=req.body.longitude;
    var radius=req.body.radius;
    var tablename="useregister";
    var filledValues=[key,lat,long,radius];
    async.waterfall([
            function(callback) {
                func.checkBlank(res, filledValues, callback);
            } ,
            function(callback) {
                func.checkKey(res, key, tablename, callback);
            }],
        function(err,userId) {
            async.waterfall([
                    function(callback) {
                        func.getNoOfDrivers(res,lat,long,radius,callback);
                    }],
                function(err,driverinfo,AndroidIds,Iosids) {
                    func.sendNotification(res,driverinfo,AndroidIds,Iosids,userId);
                }
            );

        }
    );
});
router.post('/nearestDriver',function(req,res){
    var key=req.body.access_token;
    var lat=req.body.latitude;
    var long=req.body.longitude;
    var tablename="useregister";
    var filledValues=[key,lat,long];
    async.waterfall([
            function(callback) {
                func.checkBlank(res, filledValues, callback);
            } ,
            function(callback) {
                func.checkKey(res, key, tablename, callback);
            }],
        function(err,userId)
        {
            async.waterfall([
                    function(callback) {
                        func.nearestDriver(res,lat,long,callback);
                    }],
                function(err,driverinfo,AndroidIds,Iosids) {
                    func.sendNotification(res, driverinfo, AndroidIds, Iosids,userId);
                }
            );

        }
    );
});
router.post('/accept_order',function(req,res){
   var customer_id=req.body.userID;
    var key=req.body.access_token;
    var values=[customer_id,key];
    var usertable="useregister";
    var driver_table="driveregister";
        async.waterfall([
            function(callback) {
              func.checkBlank(res,values,callback);
            },
            function(callback) {
                func.checkKey(res,key,driver_table,callback);
            }],
            function(err,driverId) {
              var status_query="select user_Id from "+usertable+" where order_status=1 and user_id='"+customer_id+"'";
              dbConnection.Query(res,status_query,[], function (userResponse) {
                  if (userResponse.length > 0) {
                      var msg = "order already done";
                      sendResponse.sendErrorMessage(msg, res);
                  }
                  else {
                      var order_query = "update " + usertable + " set order_status=1 where user_id='" + customer_id + "'";
                      dbConnection.Query(res, order_query, [], function (userResponse) {
                          if (userResponse.affectedRows) {
                              sendResponse.sendSuccessMessage(res);
                          }
                          else{
                             sendResponse.somethingWentWrongError(res);
                          }
                      });
                  }
              });
            }
        );
    });
router.post('/reject_order',function(req,res){
    var customer_id=req.body.userID;
    var key=req.body.access_token;
    var values=[customer_id,key];
    var usertable="useregister";
    var driver_table="driveregister";
    async.waterfall([
            function(callback) {
                func.checkBlank(res,values,callback);
            },
            function(callback) {
                func.checkKey(res,key,driver_table,callback);
            }],
        function(err,driverId) {
            var status_query="select user_Id from "+usertable+" where order_status=1 and user_id='"+customer_id+"'";
            dbConnection.Query(res,status_query,[], function (userResponse) {
                    if(userResponse.length>0) {
                        var msg="order already done";
                        sendResponse.sendErrorMessage(msg,res);
                    }
                    else {
                        var order_query="update "+usertable+" set order_status=0 where user_id='"+customer_id+"'";
                        dbConnection.Query(res,order_query,[], function (userResponse) {
                            if(userResponse.affectedRows){
                              sendResponse.sendSuccessMessage(res);
                            }
                            else{
                              sendResponse.somethingWentWrongError(res);
                            }
                        });
                    }
                }
            );
        }
    );
});
module.exports = router;