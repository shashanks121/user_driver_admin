var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.render('page');
});

/* GET test page. */
router.get('/login_user', function(req, res) {
  res.render('test');
});
router.get('/changePassword',function(req,res){
  res.render('userDriverForgot');
});

module.exports = router;