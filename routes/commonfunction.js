var sendResponse = require('./sendResponse');
var smartId=require('smart-id');
var nodemailer =require('nodemailer');
var geolib=require('geolib');
var gcm=require('multi-gcm');
var MultiGCM = new gcm.MultiGCM('AIzaSyCtRs4zCcJWT9d9fMutZcrLZzBdxxYY8Xs');
/*
 * ------------------------------------------------------
 * Check if manadatory fields are not filled
 * INPUT : array of field names which need to be mandatory
 * OUTPUT : Error if mandatory fields not filled
 * ------------------------------------------------------
 */
exports.checkBlank = function (res, manValues, callback) {

    var checkBlankData = checkBlank(manValues);

    if (checkBlankData) {
        sendResponse.parameterMissingError(res);
    }
    else {
        callback(null);
    }
};

function checkBlank(arr) {

    var arrlength = arr.length;

    for (var i = 0; i < arrlength; i++) {
        if (arr[i] == '') {
            return 1;
            break;
        }
        else if (arr[i] == undefined) {
            return 1;
            break;
        }
        else if (arr[i] == '(null)') {
            return 1;
            break;
        }
    }
    return 0;
}

exports.confirmPassword=function(res,password,confirmPassword,callback){
    if(password==confirmPassword){
        callback();
    }
    else{
       sendResponse.invalidInputs(res);
    }
};

/*
 * ------------------------------------------------------
 *  check App Version
 *  INPUT : appVersion
 *  OUTPUT : update popup and critical
 * ------------------------------------------------------
 *
 */
/*
exports.checkAppVersion = function (res,deviceType, appVersion, callback) {

    var sql = "SELECT `id`, `type`, `version`, `critical`,`last_critical_version` FROM `app_version` WHERE `type`=? limit 1";
    var values = [deviceType];
    dbConnection.Query(res, sql, values, function (appVersionResponse) {

        console.log(appVersionResponse);

        appVersion = parseInt(appVersion);

        if(appVersionResponse[0].last_critical_version > appVersion)
        {
            callback(null, 1, 1);
        }
        else if (appVersionResponse[0].version > appVersion) {
            callback(null, 1, appVersionResponse[0].critical);
        }
        else {
            callback(null, 0, 0);
        }
    });
};
*/
/*
 * -----------------------------------------------------------------------------
 * Encryption code
 * INPUT : string
 * OUTPUT : crypted string
 * -----------------------------------------------------------------------------
 */
exports.encrypt = function (text) {

    var crypto = require('crypto');
    var cipher = crypto.createCipher('aes-256-cbc', 'd6F3Efeq');
    var crypted = cipher.update(text, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
};


/*
 * ------------------------------------------------------
 * Authenticate a user through Access token and return extra data
 * Input:Access token{Optional Extra data}
 * Output: User_id Or Json error{Optional Extra data}
 * ------------------------------------------------------
 */
/*exports.authenticateAccessTokenAndReturnExtraData = function (accesstoken, arr, res, callback) {

    var sql = "SELECT `user_id`";
    arr.forEach(function (entry) {
        sql += "," + entry;
    });
    sql += " FROM `users`";
    sql += " WHERE `access_token`=? LIMIT 1";
    var values = [accesstoken];
    dbConnection.Query(res, sql, values, function (result) {

        if (result.length > 0) {

            return callback(null, result);

        } else {
            sendResponse.invalidAccessTokenError(res);
        }
    });

}
*/
exports.checkEmailAvailability=function(res,email,tablename,callback){

    var sql = "SELECT `first_name` FROM "+tablename+" WHERE `email`= ? limit 1";
    var values = [email];

    dbConnection.Query(res, sql, values, function (userResponse) {
        if (userResponse.length) {
            var errorMsg = 'Email already exists!';
            sendResponse.sendErrorMessage(errorMsg, res);
        }
        else {
            callback(null);
        }
    });

};
exports.checkingEmailPassword=function(res,email,tablename,password,callback){
    var sql = "SELECT `accessToken` FROM "+tablename+" WHERE `email`= ? and `password`= ? limit 1";
    var values=[email,password];
    dbConnection.Query(res, sql, values, function (userResponse) {
        if (userResponse.length>0) {
            callback(null,userResponse);
        }
        else {
            var msg="email and password is not match";
            sendResponse.sendErrorMessage(msg,res);
        }
    });
};
exports.checkKey= function(res,key,tablename,callback){
    var auth="SELECT user_Id FROM "+tablename+" where accessToken=? LIMIT 1";
    dbConnection.Query(res,auth,key, function (userResponse) {
    if(userResponse.length>0){
        callback(null,userResponse[0].user_Id);
    }
        else{
        sendResponse.invalidAccessTokenError(res);
    }
    });
};
exports.sendRecoveryMail=function(res,email,tablename){
var checkemail="SELECT accessToken FROM "+tablename+" WHERE email=? LIMIT 1";
    dbConnection.Query(res,checkemail,email, function (userResponse) {
        if(userResponse.length){
            var tempToken=smartId.make();
            var updateTempToken = 'UPDATE '+tablename+' set tempToken= "'+tempToken+'" WHERE email="'+email +'"LIMIT 1';
            dbConnection.Query(res,updateTempToken,[], function (Response) {
                if(Response)
            console.log("token generate");
            });
            var transporter = nodemailer.createTransport({
                service: 'Gmail',
                auth: {
                    user: 'shashank.sharma@theroboticshomepage.com',
                    pass: 'jugnoo123'
                }
            });
            var mailOptions = {
                from: 'Shashank Sharma  <shashank.sharma@mail.click-labs.com>', // sender address
                to: email, // list of receivers
                subject: 'confirm email', // Subject line
                text: 'Done ?', // plaintext body
                html: 'Your account password has been sucessfully reset. Use this link[access code] to change.<br><b>recovery link: http://localhost:8001/changePassword?accessKey='+tempToken+' </b>'// html body
            };
            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    var msg="email is not send due to some error";
                    sendResponse.sendErrorMessage(msg,res);
                    return console.log(error);
                }
                console.log('Message sent: ' + info.response);
            });
            var msg="email sent successfully";
            sendResponse.successStatusMsg(msg,res);
        }
        else{
            var errorMsg = 'Email does not exists!';
            sendResponse.sendErrorMessage(errorMsg, res);
        }
    });
};
exports.checkTempToken=function(res,tempToken,tablename,callback){
    var auth="SELECT user_Id FROM "+tablename+" where tempToken=? LIMIT 1";
    dbConnection.Query(res,auth,tempToken, function (userResponse) {
        if(userResponse.length>0){
            var changeToken = "UPDATE "+tablename+" set tempToken='' WHERE tempToken='"+tempToken+"' limit 1";
            dbConnection.Query(res, changeToken, [], function (changeResponse) {
                if(changeResponse.affectedRows){
                    callback(null,userResponse[0].user_Id);
                }
                else{
                    var msg="token is not deleted";
                    sendResponse.sendErrorMessage(msg,res);
                }
            });
        }
        else{
            sendResponse.invalidAccessTokenError(res);
        }
    });
};
exports.changePassword=function(res,new_password,userID,tablename){
   var updatePassword = "UPDATE "+tablename+" set password='"+new_password+"' WHERE user_Id='"+userID+"' limit 1";
    dbConnection.Query(res,updatePassword,[], function (updateResponse) {
        var msg = "";
        if (updateResponse.affectedRows) {
            msg = "password is update";
            sendResponse.successStatusMsg(msg, res);
        }
        else {
            msg = "password is not update";
            sendResponse.sendErrorMessage(msg, res);
        }

    });
};
exports.oldPasswordUpdate=function(res,userId,oldEncryptPassword,newEncryptPassword,tablename){
    var checkPassword="select user_Id from "+tablename+" where user_Id='"+userId+"' and password='"+oldEncryptPassword+"'";
    dbConnection.Query(res,checkPassword,[], function (checkResponse) {
        if(checkResponse.length) {
            var updatePassword = "UPDATE "+tablename+" set password='"+newEncryptPassword+"' WHERE user_Id='"+userId+"' limit 1";
            dbConnection.Query(res,updatePassword,[], function (updateResponse) {
                var msg="";
                if(updateResponse.affectedRows) {
                     msg="password is update";
                    sendResponse.successStatusMsg(msg,res);
                }
                else{
                     msg="password is not update";
                    sendResponse.sendErrorMessage(msg,res);
                }
            });

        }
        else{
            var msg="old password is not correct";
            sendResponse.sendErrorMessage(msg,res);
        }
    });
};
exports.getNoOfDrivers=function(res,lat,long,radius,callback){
    var androidIds=[];
    var IosIds=[];
    var driverLocation="SELECT user_Id,first_name,last_name,email,status,latitude,longitude,device_type,device_token from driveregister where status=1 ";
    dbConnection.Query(res,driverLocation,[], function (userResponse) {
            var driverData=[];
            var count=0;
            for(var i=0;i<userResponse.length;i++) {
                var latDriver =userResponse[i].latitude;
                var longDriver =userResponse[i].longitude;
                var range = geolib.isPointInCircle(
                    {latitude: latDriver, longitude: longDriver},
                    {latitude: lat, longitude: long},
                    radius
                );
                if(range==true){
                    driverData[count]={
                        driver_id:userResponse[i].user_Id,
                        firstname:userResponse[i].first_name,
                        lastname:userResponse[i].last_name,
                        latitude:userResponse[i].latitude,
                        longitude:userResponse[i].longitude,
                        email:userResponse[i].email,
                        status:userResponse[i].status
                    };
                    if(userResponse[i].device_type==1) {
                        androidIds.push(userResponse[i].device_token);
                    }
                    else if (userResponse[i].device_type==2) {
                        IosIds.push(userResponse[i].device_token);
                    }
                    else {
                        /*var msg = "incompatible device";
                         sendResponse.sendErrorMessage(msg, res);*/
                    }
                    count++;
                }
            }
            callback(null,driverData,androidIds,IosIds);
        }
    );
};
exports.nearestDriver=function(res,lat,long,callback){
    var androidIds=[];
    var IosIds=[];
    var driverData=[];
    var nearestDriver="";
    var driverLocation="SELECT user_Id,first_name,last_name,email,status,latitude,longitude,device_type,device_token from driveregister where status=1 ";
    dbConnection.Query(res,driverLocation,[], function (userResponse) {
        var userLocation={
            latitude:lat,
            longitude:long
        };
       nearestDriver=geolib.findNearest(userLocation,userResponse, 0)
        var distance=geolib.convertUnit('km',nearestDriver.distance,2);
            for(var i=0;i<userResponse.length;i++) {
                if((userResponse[i].latitude==nearestDriver.latitude)&&(userResponse[i].longitude==nearestDriver.longitude)) {
                    if(userResponse[i].device_type==1) {
                        androidIds.push(userResponse[i].device_token);
                    }
                    else if(userResponse[i].device_type==2) {
                        IosIds.push(userResponse[i].device_token);
                    }
                    else {

                    }
                    driverData.push({
                        driver_id:userResponse[i].user_Id,
                        firstname:userResponse[i].first_name,
                        lastname:userResponse[i].last_name,
                        latitude:userResponse[i].latitude,
                        longitude:userResponse[i].longitude,
                        email:userResponse[i].email,
                        status:userResponse[i].status,
                        distanceFromLocation:distance
                    });
                }
                else {

                }
            }
            callback(null,driverData,androidIds,IosIds);
        }
    );
};
exports.sendNotification=function(res,driverinfo,AndroidIds,Iosids,userId){
    var messageAndroid = new gcm.GCMMessage({
        registration_ids: AndroidIds,
        type: 'Android', //Android or iOS
        collapse_key: 'demo',
        priority: 'high',
        delay_while_idle: true,
        time_to_Live: 10,
        restricted_package_name: "driverNotification",
        dry_run: true,
        data: {
            news: 'customer',
            customer_id:userId
        },
        notification: {
            title: "customer available",
            icon: "ic_launcher",
            body: "accept or decline order"
        }
    });
    var messageIos = new gcm.GCMMessage({
        registration_ids: Iosids,
        type: 'ios', //Android or iOS
        collapse_key: 'demo',
        priority: 'high',
        delay_while_idle: true,
        time_to_Live: 10,
        restricted_package_name: "driverNotification",
        dry_run: true,
        content_available: true,
        data: {
            news: 'customer',
            customer_id:userId
        },
        notification: {
            body: "accept or decline order"
        }
    });
    if(AndroidIds.length) {
        MultiGCM.send(messageAndroid, function (err, response) {
            if (err) {
                console.error(err);
            }
            else {
                console.log(response); //Containing the custom result object of this Library
                console.log(response.result); //Containing the JSON object response from GCM
            }
        });
    }
    if(Iosids.length) {
        MultiGCM.send(messageIos, function (err, response) {
            if (err) {
                console.error(err);
            } else {
                console.log(response); //Containing the custom result object of this Library
                console.log(response.result); //Containing the JSON object response from GCM
            }
        });
    }
    var data={
        msg:"total no of Drivers:"+driverinfo.length,
        driverinfo:driverinfo
    };
    sendResponse.sendSuccessData(data,res);
};